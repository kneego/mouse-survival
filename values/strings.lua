local values = {
	-- constants
	admob_id = {
		['iPhone OS'] = 'ca-app-pub-1210274935500874/4432463144',
		['Android'] = 'ca-app-pub-1210274935500874/3416401542'
	},

	board_id = {
		apple = "topscore",
		android = 'CgkI09Oy6JoDEAIQAQ',
	},

	score_achievement_id = {
		['iPhone OS'] = {
			{score = 25, id = 'score_25'},
			{score = 50, id = 'score_50'},
			{score = 100, id = 'score_100'},
			{score = 200, id = 'score_200'},
			{score = 300, id = 'score_300'},
			{score = 500, id = 'score_500'},
			{score = 1000, id = 'score_1000'},
		},
		['Android'] = {
			{score = 25, id = 'CgkI09Oy6JoDEAIQCA'},
			{score = 50, id = 'CgkI09Oy6JoDEAIQAg'},
			{score = 100, id = 'CgkI09Oy6JoDEAIQAw'},
			{score = 200, id = 'CgkI09Oy6JoDEAIQBA'},
			{score = 300, id = 'CgkI09Oy6JoDEAIQBQ'},
			{score = 500, id = 'CgkI09Oy6JoDEAIQBg'},
			{score = 1000, id = 'CgkI09Oy6JoDEAIQBw'},
		}
	},

	facebook_url = "https://m.facebook.com/MouseFamilyGame",

	-- about
	kneegoLink = "www.kneego.org",
	kneegoLinkTouch = "http://www.kneego.org",

	leaderboards_head = "Leaderboards",
}

return values