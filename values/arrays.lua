local game_over_text = {
	'Yuppie...\nMom is back. We were sooo hungry...',
	'Hey kids.\nI got some food for ya.',
	"Big bad cat is speeding up...\nBut mom is still faster. Let's eat!",
	'I got cheese, nuts and some bread.\nMouse feast begins!',
	'Big bad cat went insane.\nI was lucky...',
	'Hold on kids.\nMom is going back to get some more food.',
	'There is a poison out there.\nIt can cost me life!!! Eat my darlings.',
	"I saw apple and tasty bacon.\nNext time I'll bring some more.",
	'Oh my GOD!!! Trap is there!!!\nAnd cat runs still faster.',
	'THIS is madness kids!!!\nI must be the most fastest mouse on Earth.',
}

local arrays = {
	game_over_text = game_over_text,
}

return arrays