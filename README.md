# Mouse Survival

Full source code and assets of Mouse Survival game for [Android](https://play.google.com/store/apps/details?id=org.kneego.themousefamily) and [iOS](https://itunes.apple.com/us/app/the-mouse-family/id943930459?ls=1&mt=8).

The game uses the [Corona SDK](https://coronalabs.com) game engine.
