-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------
local composer = require( "composer" )
local scene = composer.newScene()


function scene:create( event )
	local sceneGroup = self.view

	settings.show_guide = false
	utils.saveSettings(settings)

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW, screenH )
	background.anchorX = 0
	background.anchorY = 0
	background.x, background.y = 0, 0
	sceneGroup:insert( background )
	
	-- 1. bod
	local guide_1 = display.newImageRect( 'images/guide-1.png', 181, 152 )
	guide_1.anchorX = 0
	guide_1.x = 0
	guide_1.y = (screenH / 4)
	sceneGroup:insert( guide_1 )

	-- 2. bod
	local guide_2 = display.newImageRect( 'images/guide-2.png', 181, 153 )
	guide_2.x = screenW / 2
	guide_2.y = (screenH / 4) + 5
	sceneGroup:insert( guide_2 )

	-- 3. bod
	local guide_3 = display.newImageRect( 'images/guide-3.png', 181, 154 )
	guide_3.anchorX = 1
	guide_3.x = screenW
	guide_3.y = (screenH / 4) - 5
	sceneGroup:insert( guide_3 )

	-- 4. bod
	local guide_4 = display.newImageRect( 'images/guide-4.png', 181, 154 )
	guide_4.anchorX = 0
	guide_4.x = 0
	guide_4.y = (screenH / 4 * 3) - 30
	sceneGroup:insert( guide_4 )

	-- 5. bod
	local guide_5 = display.newImageRect( 'images/guide-5.png', 181, 154 )
	guide_5.x = screenW / 2
	guide_5.y = (screenH / 4 * 3) - 15
	sceneGroup:insert( guide_5 )

	-- 6. bod
	local guide_6 = display.newImageRect( 'images/guide-6.png', 181, 154 )
	guide_6.anchorX = 1
	guide_6.x = screenW
	guide_6.y = (screenH / 4 * 3) - 30
	sceneGroup:insert( guide_6 )

	-- po urcitom case zobrazime button na pokracovanie
	timer.performWithDelay( 2000, function ( ... )
		local touch_to_continue = display.newText( {
			text = 'Tap to continue...',
			x = screenW * 0.5,
			y = screenH - 18,
			fontSize = 28,
			font = 'Melapalooza'
		} )
		touch_to_continue:setFillColor( 0, 0, 0 )
		sceneGroup:insert( touch_to_continue )

		utils.handlerAdd(background, 'touch', function ( self, event )
			if event.phase == 'began' then
				composer.gotoScene('game')
			end
		end)
	end )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		
	elseif phase == "did" then
		ga:view('game-guide')
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene