local _M = {
	music_sound = audio.loadSound( "sounds/music.mp3" ),
	cat_sound = audio.loadSound( "sounds/cat.mp3" ),
	mouse_sound = audio.loadSound( "sounds/mouse-run.mp3" ),
	eat_sound = audio.loadSound( "sounds/eat.mp3" ),
	door_close_sound = audio.loadSound( "sounds/door-close.mp3" ),
	door_open_sound = audio.loadSound( "sounds/door-open.mp3" ),
	mouse_in_house_sound = audio.loadSound( "sounds/mouse-in-house.mp3" ),
	trap_sound = audio.loadSound( "sounds/trap.mp3" ),
	cat_vs_mouse = audio.loadSound( "sounds/cat-vs-mouse.mp3" ),
}

return _M