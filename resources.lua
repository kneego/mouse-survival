local _strings = require( "values.strings" )
local _arrays = require( "values.arrays" )

local function strings( name )
	return _strings[name]
end

local function arrays( name )
	return _arrays[name]
end

return {
	strings = strings,
	arrays = arrays
}