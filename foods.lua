local _M = {}

local _food_definition = {
	{
		image = 'images/food-cheese.png',
		width = 64,
		height = 73,
		points = 1
	},
	{
		image = 'images/food-nutt.png',
		width = 54,
		height = 58,
		points = 1
	},
	{
		image = 'images/food-bread.png',
		width = 84,
		height = 67,
		points = 2
	},
	{
		image = 'images/food-apple.png',
		width = 84,
		height = 67,
		points = 2
	},
	{
		image = 'images/food-bacon.png',
		width = 150,
		height = 66,
		points = 3
	},
}

function _M.get_next_food(group, score, x, y)
	local _random = 2

	if score > 70 then
		-- aj slanina
		_random = 5
	elseif score > 40 then
		-- aj jablko
		_random = 4	
	elseif score > 25 then
		-- aj chlieb
		_random = 3
	else
		-- syr a oriesok
		_random = 2
	end

	local selected_food = _food_definition[math.random(_random)]

	-- najprv zobrazime mensie
	local food = display.newImageRect( group, selected_food.image, selected_food.width * 0.8, selected_food.height * 0.8 )
	food.x = x
	food.y = y

	food.points = selected_food.points

	-- zvacsime do vacsieho
	local t = transition.to( food, {
		time = 80,
		width = selected_food.width * 1.2,
		height = selected_food.height * 1.2,

		-- dame na normalnu velkost
		onComplete = function ( obj )
			if obj == nil then return end

			transition.to( obj, {
				time = 100,
				width = selected_food.width,
				height = selected_food.height,
			} )
		end
	} )

	return food
end

return _M