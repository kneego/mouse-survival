-----------------------------------------------------------------------------------------
--
-- settings.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local music
local sounds
local vibrations
local sceneGroup

function sounds_button_click( event )
	if settings.sounds then
		settings.sounds = false
	else
		settings.sounds = true
	end

	utils.saveSettings( settings )

	show_sounds_button()
end

function vibrations_button_click( event )
	if settings.vibrations then
		settings.vibrations = false
	else
		settings.vibrations = true
	end

	utils.saveSettings( settings )

	show_vibrations_button()
end

function music_button_click( event )
	if settings.music then
		settings.music = false
	else
		settings.music = true
	end

	utils.saveSettings( settings )

	show_music_button()
end

function show_music_button()
	-- utils.debug('game-settings:show_vibrations_button')

	-- remove old image
	if music ~= nil then music:removeSelf( ); music = nil end

	local music_image
	if settings.music then music_image = 'images/on.png' else music_image = 'images/off.png' end

	music = display.newImageRect( music_image, 149, 100 )
	music.anchorX = 0
	music.x = 65
	music.y = 140

	utils.handlerAdd(music, 'touch', function ( self, event )
		if event.phase == 'began' then
			music_button_click()
		end
	end )
end

function show_sounds_button()
	-- remove old image
	if sounds ~= nil then sounds:removeSelf( ); sounds = nil end

	local sounds_image
	if settings.sounds then sounds_image = 'images/on.png' else sounds_image = 'images/off.png' end

	sounds = display.newImageRect( sounds_image, 149, 100 )
	sounds.anchorX = 0
	sounds.x = 65
	sounds.y = 210

	utils.handlerAdd(sounds, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds_button_click()
		end
	end )
end

function show_vibrations_button()
	-- utils.debug('game-settings:show_vibrations_button')

	-- remove old image
	if vibrations ~= nil then vibrations:removeSelf( ); vibrations = nil end

	local vibrations_image
	if settings.vibrations then vibrations_image = 'images/on.png' else vibrations_image = 'images/off.png' end

	vibrations = display.newImageRect( vibrations_image, 149, 100 )
	vibrations.anchorX = 0
	vibrations.x = 65
	vibrations.y = 280

	utils.handlerAdd(vibrations, 'touch', function ( self, event )
		if event.phase == 'began' then
			vibrations_button_click()
		end
	end )
end

function scene:create( event )
	sceneGroup = self.view

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW, screenH )
	background.anchorX = 0
	background.anchorY = 0
	background.x, background.y = 0, 0
	sceneGroup:insert( background )
	
	-- logo
	local logo = display.newImageRect( 'images/logo.png', 340, 71 )
	logo.anchorX = 0
	logo.x = 65
	logo.y = 40
	sceneGroup:insert( logo )

	-- back button
	local back = display.newImageRect( 'images/back.png', 108, 108 )
	back.x = 40
	back.y = 40
	sceneGroup:insert( back )

	utils.handlerAdd(back, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'menu' )
		end
	end )

	-- settings label
	local wheel = display.newImageRect( 'images/wheel.png', 103 / 2, 108 / 2 )
	wheel.anchorX = 0
	wheel.x = 65
	wheel.y = 75
	sceneGroup:insert( wheel )

	local labelSettings = display.newText( {
		text = 'SETTINGS',
		fontSize = 22,
		font = 'Melapalooza',
		x = 155,
		y = 80
	} )
	labelSettings:setFillColor( 0, 0, 0 )
	sceneGroup:insert( labelSettings )

	-- sounds
	local labelMusic = display.newText( {
		text = 'MUSIC',
		fontSize = 24,
		font = 'Melapalooza',
		x = 210,
		y = 130
	} )
	labelMusic.anchorX = 0
	labelMusic:setFillColor( 0, 0, 0 )
	sceneGroup:insert( labelMusic )

	utils.handlerAdd(labelMusic, 'touch', function ( self, event )
		if event.phase == 'began' then
			music_button_click()
		end
	end )

	-- sounds
	local labelSounds = display.newText( {
		text = 'SOUNDS',
		fontSize = 24,
		font = 'Melapalooza',
		x = 210,
		y = 200
	} )
	labelSounds.anchorX = 0
	labelSounds:setFillColor( 0, 0, 0 )
	sceneGroup:insert( labelSounds )

	utils.handlerAdd(labelSounds, 'touch', function ( self, event )
		if event.phase == 'began' then
			sounds_button_click()
		end
	end )

	-- vibrations
	local labelVibrations = display.newText( {
		text = 'VIBRATIONS',
		fontSize = 24,
		font = 'Melapalooza',
		x = 210,
		y = 270
	} )
	labelVibrations.anchorX = 0
	labelVibrations:setFillColor( 0, 0, 0 )
	sceneGroup:insert( labelVibrations )

	utils.handlerAdd(labelVibrations, 'touch', function ( self, event )
		if event.phase == 'began' then
			vibrations_button_click()
		end
	end )
end

function scene:show( event )
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		show_music_button()
		show_vibrations_button()
		show_sounds_button()

		ga:view('settings')
	end	
end

function scene:hide( event )
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		if music ~= nil then music:removeSelf( ); music = nil end
		if sounds ~= nil then sounds:removeSelf( ); sounds = nil end
		if vibrations ~= nil then vibrations:removeSelf( ); vibrations = nil end
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene