local timer = require('timer')

local _M = {}

function _M._animate(o, width, height)
	-- zvacsime do vacsieho
	transition.to( o, {
		time = 80,
		width = width * 1.2,
		height = height * 1.2,

		-- dame na normalnu velkost
		onComplete = function ( ... )
			transition.to( o, {
				time = 100,
				width = width,
				height = height,

				onComplete = function ( ... )
					o.isBodyActive = trie
				end
			} )
		end
	} )

	return food
end

function _M._position()
	x = math.random( screenW - 20 ) + 10
	y = math.random( screenH - 20 ) + 10

	return x, y
end

function _M.get_trap(group, callback)
	-- najprv zobrazime mensie
	local width = 82
	local height = 75

	local trap = display.newImageRect( group, 'images/trap.png', width * 0.8, height * 0.8 )
	trap.x, trap.y = _M._position()
	trap.isBodyActive = false

	_M._animate(trap, width, height)

	-- pasca interaguje iba s mysou
	physics.addBody( trap, 'static', { bounce = 0, isSensor = true, filter={ categoryBits=16, maskBits=1 } } )

	-- handler, ktory sa vykona pri interakcii (zastavi mys na urcity cas)
	utils.handlerAdd(trap, 'collision', callback)

	-- pustime zvuk
	-- TODO

	return trap
end

function _M.get_poison(group, callback)
	-- najprv zobrazime mensie
	local width = 86
	local height = 97

	local poison = display.newImageRect( group, 'images/trap-poison.png', width * 0.8, height * 0.8 )
	poison.x, poison.y = _M._position()
	poison.isBodyActive = false

	_M._animate(poison, width, height)

	-- jed interaguje iba s mysou
	physics.addBody( poison, 'static', { bounce = 0, isSensor = true, filter={ categoryBits=32, maskBits=1 } } )

	-- handler, ktory sa vykona pri interakcii (spomali mys na urcity cas)
	utils.handlerAdd(poison, 'collision', callback)

	-- pustime zvuk
	-- TODO

	return poison
end

return _M