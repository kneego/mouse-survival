--
-- Part of KneeGo useful lib
--
-- Pavel Koci (@pavelkoci)
-- KneeGo (@KneeGoApps)
--
-- https://bitbucket.org/pavelkoci/corona-knee-lib/
--

local io = require( "io" )
local json = require( "json" )
local timer = require( "timer" )
local system = require( "system" )
local gameNetwork = require( "gameNetwork" )

debugging = false

local M = {}

function M.debug( ... )
	if debugging then
		print ('debug:', unpack(arg))
	end
end

function M.warn( ... )
	print ('warn:', unpack(arg))
end

function M.handlerAdd(object, event, handler)
	object[event] = handler
	object:addEventListener(event)
end

function M.handlerRemove(object, event, handler)
	object:removeEventListener(event, handler)
end

function M.nextFrame(f)
	timer.performWithDelay( 1, f )
end

function M.loadSettings( options )
	local customOptions = options or {}

	local opt = {}
	opt.path = customOptions.path or 'settings.json'
	opt.emptyData = customOptions.emptyData

	local path = system.pathForFile( opt.path, system.DocumentsDirectory )
	local file = io.open( path, "r" )

	if file then
		-- load state
		local savedDate = file:read( "*a" )
		
		io.close( file )
		
		file = nil

		return json.decode( savedDate )
	else
		-- create empty data and save it
		local emptyData
		if opt.emptyData and type(empty) == 'function' then
			emptyData = opt.emptyData()
		elseif opt.emptyData then
			emptyData = opt.emptyData
		else
			emptyData = {}
		end

		M.saveSettings(emptyData)

		return emptyData
	end
end

function M.saveSettings( data )
	local customOptions = options or {}

	local opt = {}
	opt.path = customOptions.path or 'settings.json'
	
	local jsonData = json.encode( data )

	local path = system.pathForFile( opt.path, system.DocumentsDirectory )
	local file = io.open( path, "w" )

	file:write( jsonData )

	io.close( file )

	file = nil
end

function M.test_network_connection()
    local netConn = require('socket').connect('www.apple.com', 80)
    
    if netConn == nil then
        return false
    end

    netConn:close()
    return true
end

return M