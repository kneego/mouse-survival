-- globals.lua

local system = require( 'system' )
local display = require( "display" )

screenW = display.contentWidth
screenH = display.contentHeight

halfW = display.contentWidth * 0.5
halfH = display.contentHeight * 0.5

platform_name = system.getInfo('platformName')

is_android = (platform_name == 'Android')
is_ios = (platform_name == 'iPhone OS')
is_emulator = (platform_name == 'MAC OS')