--
-- Corona Google Analytics module
-- Pavel Koci (@pavelkoci) (KneeGo (@KneeGoApps))
--
-- https://bitbucket.org/pavelkoci/corona-ga-module/
--

local system = require( "system" )
local network = require( "network" )
local display = require( 'display' )

local version = 1

local function escape(s)
	s = string.gsub(s, "([&=+%c])", function (c)
		return string.format("%%%02X", string.byte(c))
	end)
	s = string.gsub(s, " ", "+")
	return s
end

local function encode(data)
	local s = ""

	for k, v in pairs(data) do
		s = s .. "&" .. escape(k) .. "=" .. escape(v)
	end

	return string.sub(s, 2)
end

local function networkListener(event)
	if event.isError then
		print ("GA request failed.")
	end
end

local ga = {}

function ga:init(gaID)
	self.gaID = gaID

	self.appName = system.getInfo( "appName" )
	self.appVersion = system.getInfo( "appVersionString" )
	self.cid = system.getInfo( "deviceID" )

	local headers = {}

	self.ua = "GoogleAnalytics/2.0 (Corona; U; " ..
		system.getInfo( "platformName" ) .. " " .. system.getInfo( "platformVersion" ) .. "; " ..
		system.getPreference( "locale", "identifier" ) .. "; " ..
		system.getInfo( "platformVersion" ) .. ")"

	self.sr = display.pixelWidth .. 'x' .. display.pixelHeight
	self.vp = screenW .. 'x' .. screenH
	
	self.params = {}
	self.params.headers = headers
end

function ga:event(category, action, label, value)
	self:sendData( {
		t = "event",
		ec = category,
		ea = action,
		el = label,
		ev = value
	} )
end

function ga:view(screenName)
	self:sendData( {
		t = "appview",
		cd = screenName
	} )
end

function ga:sendData( data )
	data.v = version
	data.tid = self.gaID
	data.cid = self.cid
	data.an = self.appName
	data.av = self.appVersion
	data.ua = self.ua
	data.sr = self.sr
	data.vp = self.vp

	payload = encode( data )

	network.request(
		"http://www.google-analytics.com/collect?" .. payload,
		"POST",
		networkListener,
		self.params
	)
end

return ga