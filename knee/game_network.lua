local json = require('json')
local gameNetwork = require( 'gameNetwork' )

local _M = {}

-- logged in status
_M.logged_in = false

_M.platform_name = platform_name
_M.init_error_message = 'Error logging in.\nTry it later.'

function _M._default_init_callback( event )
	if event and event.errorCode then
		-- something went wrong
		if _M.init_error_message then
			native.showAlert( 'Network error', _M._init_score_error_message, {'OK'} )
		end

		_M.logged_in = false

		return false
	end

	if event and event.data then
		-- user is logged in, load score
		_M.logged_in = true

		if callback ~= nil then
			callback(event)
		end
	end
end

function _M.init(callback, user_initiated)
	local _service_name = ''

	local _user_initiated
	if user_initiated ~= nil then _user_initiated = user_initiated else _user_initiated = true end

	local _callback
	if callback ~= nil then _callback = callback else _callback = _M._default_init_callback end

	if _M.platform_name == 'iPhone OS' then
		_service_name = 'gamecenter'
	elseif _M.platform_name == 'Android' then
		_service_name = 'google'
	end

	if _service_name ~= '' then
		local data = gameNetwork.init( _service_name, _callback )

		if not data then
			-- user is logged in to game center
			_M.logged_in = true

			if _callback ~= nil then
				_callback()
			end
		end
		
		if is_android then
			-- log in user
			_M.logged_in = false

			gameNetwork.request("login",
			{
				userInitiated = _user_initiated,
				listener = _callback
			})
		end
	end
end

function _M.send_score(board_id, score, callback)
	local _board_id
	if _M.platform_name == 'iPhone OS' then
		_board_id = board_id.apple
	elseif _M.platform_name == 'Android' then
		_board_id = board_id.android
	end

	if _board_id ~= '' then
		gameNetwork.request( "setHighScore", {
			localPlayerScore = { category=_board_id, value=score },
			listener = callback
		})
	end
end

function _M.show_leaderboards()
	gameNetwork.show( 'leaderboards', { 
		leaderboard = {timeScope='Week'},
	} )
end

function _M.unlock_achievement(identifier, callback)
	gameNetwork.request( "unlockAchievement",
		{
			achievement =
			{
				identifier = identifier
			},
			listener = callback
		}
	)
end

return _M