-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

require( "knee.globals" )

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- data = require( "data" )
ads = require( 'ads' )
utils = require( "knee.utils" )
back_button = require( 'knee.back_button' )
game_network = require( 'knee.game_network' )

-- resources (strings, arrays)
R = require( "resources" )
sounds = require('sounds')

ga = require( "knee.ga" )
ga:init( "UA-39791763-12" )

-- reklamu zobraujeme inu pre iOS a inu pre Android
ads.init( 'admob', R.strings('admob_id')[platform_name])

back_button.init()

debugging = true

-- load settings
settings = utils.loadSettings({
	-- default empty data
	emptyData = {
		vibrations = true,
		sounds = true,
		music = true,
		show_guide = true,
		best_score = 0,
		game_play_counter = 0,
	}
})

local composer = require "composer"
composer.gotoScene( "menu" )