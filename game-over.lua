-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------
local composer = require( "composer" )
local scene = composer.newScene()

local score = 0

local score_value
local best_score_value

function scene:create( event )
	local sceneGroup = self.view

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW, screenH )
	background.anchorX = 0
	background.anchorY = 0
	background.x, background.y = 0, 0
	sceneGroup:insert( background )
	
	-- game over background
	local logo = display.newImageRect( 'images/game-over-background.png', 279, 286 )
	logo.x = screenW / 2
	logo.y = screenH / 2
	sceneGroup:insert( logo )

	local label_game = display.newText( {
		text = 'GAME',
		fontSize = 64,
		font = 'Melapalooza',
		x = screenW / 2 ,
		y = 94
	} )
	label_game:setFillColor( 0, 0, 0 )
	sceneGroup:insert( label_game )

	local label_over = display.newText( {
		text = 'OVER',
		fontSize = 64,
		font = 'Melapalooza',
		x = screenW / 2 ,
		y = 136
	} )
	label_over:setFillColor( 0, 0, 0 )
	sceneGroup:insert( label_over )

	-- main menu
	local main_menu = display.newImageRect( 'images/main-menu.png', 112, 121 )
	main_menu.x = screenW / 2 - 180
	main_menu.y = screenH / 2
	sceneGroup:insert( main_menu )

	utils.handlerAdd(main_menu, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'menu' )
		end
	end )

	local label_main_menu = display.newText( {
		text = 'MAIN MENU',
		fontSize = 22,
		font = 'Melapalooza',
		x = screenW / 2 - 180,
		y = screenH / 2 + 60
	} )
	label_main_menu:setFillColor( 0, 0, 0 )
	sceneGroup:insert( label_main_menu )

	utils.handlerAdd(label_main_menu, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'menu' )
		end
	end )

	-- play again
	local play_again = display.newImageRect( 'images/play-again.png', 113, 105 )
	play_again.x = screenW / 2 + 180
	play_again.y = screenH / 2
	sceneGroup:insert( play_again )

	utils.handlerAdd(play_again, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'game' )
		end
	end )

	local label_play_again = display.newText( {
		text = 'PLAY AGAIN',
		fontSize = 22,
		font = 'Melapalooza',
		x = screenW / 2 + 180,
		y = screenH / 2 + 60
	} )
	label_play_again:setFillColor( 0, 0, 0 )
	sceneGroup:insert( label_play_again )

	utils.handlerAdd(label_play_again, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'game' )
		end
	end )

	-- score
	local score_bg = display.newImageRect( 'images/score-background.png', 92, 50 )
	score_bg.x = screenW / 2 + 40
	score_bg.y = screenH / 2 + 40
	sceneGroup:insert( score_bg )

	score_value = display.newText( {
		text = '',
		fontSize = 18,
		font = 'Melapalooza',
		x = screenW / 2 + 40,
		y = screenH / 2 + 40
	} )
	score_value:setFillColor( 0, 0, 0 )
	sceneGroup:insert( score_value )

	local score_label = display.newText( {
		text = 'SCORE',
		fontSize = 18,
		font = 'Melapalooza',
		x = screenW / 2 - 10,
		y = screenH / 2 + 40
	} )
	score_label:setFillColor( 0, 0, 0 )
	sceneGroup:insert( score_label )

	-- best score
	local best_score_bg = display.newImageRect( 'images/best-score-background.png', 92, 50 )
	best_score_bg.x = screenW / 2 + 45
	best_score_bg.y = screenH / 2 + 70
	sceneGroup:insert( best_score_bg )

	best_score_value = display.newText( {
		text = '',
		fontSize = 18,
		font = 'Melapalooza',
		x = screenW / 2 + 45,
		y = screenH / 2 + 72
	} )
	best_score_value:setFillColor( 0, 0, 0 )
	sceneGroup:insert( best_score_value )

	local best_score_label = display.newText( {
		text = 'YOUR BEST',
		fontSize = 18,
		font = 'Melapalooza',
		x = screenW / 2 - 30,
		y = screenH / 2 + 72
	} )
	best_score_label:setFillColor( 0, 0, 0 )
	sceneGroup:insert( best_score_label )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		if event.params and event.params.score then score = event.params.score else score = 0 end
	elseif phase == "did" then
		score_value.text = score
		best_score_value.text = settings.best_score

		settings.game_play_counter = (settings.game_play_counter or 0) + 1
		utils.saveSettings(settings)

		if settings.game_play_counter % 3 == 0 then
			if ads.isLoaded("interstitial") then
				ads.show( "interstitial", { x=0, y=0, testMode = false } )
			end
		end

		ga:view('game-over')
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene