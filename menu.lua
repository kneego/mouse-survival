-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local system = require( 'system' )
local native = require('native')

function play_event_handler( self, event )
	if event.phase == 'began' then
		if settings.show_guide then
			-- zobrazime navod
			composer.gotoScene( 'game-guide' )
		else
			-- a ideme hrat
			composer.gotoScene( 'game' )
		end
	end
end

function scene:create( event )
	local sceneGroup = self.view

	-- display a background image
	local background = display.newImageRect( "images/background.jpg", screenW, screenH )
	background.anchorX = 0
	background.anchorY = 0
	background.x, background.y = 0, 0
	sceneGroup:insert( background )
	
	-- logo
	local logo = display.newImageRect( 'images/logo.png', 679 / 2, 142 / 2 )
	logo.x = screenW / 2
	logo.y = 50
	sceneGroup:insert( logo )

	-- leaderboards
	local leaderboards = display.newImageRect( 'images/leaderboards.png', 127, 148 )
	leaderboards.anchorX = 0.5
	leaderboards.anchorY = 1
	leaderboards.x = (screenW / 2) - 130
	leaderboards.y = 290
	sceneGroup:insert( leaderboards )

	utils.handlerAdd(leaderboards, 'touch', function ( self, event )
		if event.phase == 'began' then
			game_network.show_leaderboards()
		end
	end )

	local labelLeaderboards = display.newText( {
		text = 'LEADERBOARDS',
		fontSize = 22,
		font = 'Melapalooza',
		x = (screenW / 2) - 130,
		y = 272
	} )
	labelLeaderboards:setFillColor( 0, 0, 0 )
	sceneGroup:insert( labelLeaderboards )

	utils.handlerAdd(labelLeaderboards, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'leaderboards' )
		end
	end )

	-- settings
	local settings = display.newImageRect( 'images/settings.png', 123, 148 )
	settings.anchorX = 0.5
	settings.anchorY = 1
	settings.x = (screenW / 2) + 130
	settings.y = 290
	sceneGroup:insert( settings )

	utils.handlerAdd(settings, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'game-settings' )
		end
	end )

	local labelSettings = display.newText( {
		text = 'SETTINGS',
		fontSize = 22,
		font = 'Melapalooza',
		x = (screenW / 2) + 130,
		y = 272
	} )
	labelSettings:setFillColor( 0, 0, 0 )
	sceneGroup:insert( labelSettings )

	utils.handlerAdd(labelSettings, 'touch', function ( self, event )
		if event.phase == 'began' then
			composer.gotoScene( 'game-settings' )
		end
	end )

	-- play
	local play = display.newImageRect( 'images/play.png', 186, 185 )
	play.anchorX = 0.5
	play.anchorY = 1
	play.x = screenW / 2
	play.y = 270
	sceneGroup:insert( play )

	utils.handlerAdd(play, 'touch', play_event_handler )

	local labelPlay = display.newText( {
		text = 'PLAY',
		fontSize = 22,
		font = 'Melapalooza',
		x = screenW / 2,
		y = 272
	} )
	labelPlay:setFillColor( 0, 0, 0 )
	sceneGroup:insert( labelPlay )

	utils.handlerAdd(labelPlay, 'touch', play_event_handler )

	-- facebook
	local fb = display.newImageRect( 'images/fb.png', 67, 70 )
	fb.anchorX = 1
	fb.anchorY = 0
	fb.x = screenW - 5
	fb.y = 0
	sceneGroup:insert( fb )

	utils.handlerAdd(fb, 'touch', function ( self, event )
		if event.phase == 'began' then
			system.openURL( R.strings('facebook_url') )
		end
	end )

	local mouse = display.newImageRect( 'images/mouse-frame.png', 79, 196 )
	mouse.rotation = -120
	mouse.x = screenW + 5
	mouse.y = 110
	sceneGroup:insert( mouse )

	-- cheese image
	local cheese = display.newImageRect( 'images/food-cheese.png', 519 / 6, 586 / 6 )
	cheese.x = 4
	cheese.y = 268
	sceneGroup:insert( cheese )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		
		-- inicializujeme game network hned pri spusteni
		game_network.init()

		ga:view('menu')
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view
	
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene