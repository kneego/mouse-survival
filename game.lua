-----------------------------------------------------------------------------------------
--
-- game.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local widget = require( "widget" )
local transition = require( "transition" )
local math = require( "math" )
local physics = require( "physics" )
local audio = require('audio')

local foods = require('foods')
local trap = require('trap')

--------------------------------------------

local background
local mouse
local cat
local food
local food_group  -- tu budem drzat jedlo, aby bolo pod mysou a mackou
local bang
local doorOpen
local doorClosed

local house_group
local house
local house_background
local in_house = false
local label_hurray

local scoreText
local score

local foodCountText
local foodCount

local inCollision
local catMoveTimer
local catRotateTimer
local release_cat_timer
local cat_transition

local trap_group
local trap_object
local trap_timer
local trap_hide_timer

local mouseMoveTime = 1000
local mouseMoveDistance = 500
local mouse_transition = nil
local mouse_in_move = false

local catMoveTime = 8000

local COMMON_CHANNEL = 1
local MOUSE_SOUND_CHANNEL = 2
local EAT_SOUND_CHANNEL = 3
local CAT_SOUND_CHANNEL = 4
local MUSIC_CHANNEL = 5

function play_sound( sound, channel, loops)
	-- utils.debug('game:play_sound', loops)
	if settings.sounds then
		if channel == nil then channel = COMMON_CHANNEL end
		if loops == nil then loops = 0 end

		audio.play( sound, {channel = channel, loops = loops} )
	end
end

function stop_sound( channel )
	if settings.sounds then
		audio.stop( channel )
	end
end

function vibrate()
	if settings.vibrations then
		system.vibrate()
	end
end

function holeCollisionHandler( self, event )
	if event.phase == 'began' then
		utils.handlerRemove( doorOpen, 'collision' )

		enterHouse()

		utils.nextFrame(function()
			-- hide door
			doorOpen.x = -100
			doorOpen.y = -100
		end)
	end
end

function openDoor()
	-- show door
	doorOpen.x = screenW - 70
	doorOpen.y = halfH

	doorClosed.x = -100
	doorClosed.y = -100

	utils.handlerAdd( doorOpen, 'collision', holeCollisionHandler )

	play_sound(sounds.door_open_sound)
end

function closeDoor()
	-- hide door
	doorOpen.x = -100
	doorOpen.y = -100

	doorClosed.x = screenW - 70
	doorClosed.y = halfH
end

function prepare_house()
	house_group.alpha = 0

	local house_background = display.newImageRect( house_group, 'images/background.jpg', screenW, screenH )
	house_background.anchorX = 0
	house_background.anchorY = 0
	house_background.x = 0
	house_background.y = 0

	label_hurray = display.newText( {
		text = '',
		fontSize = 22,
		font = 'Melapalooza',
		x = screenW * 0.5,
		y = 40,
		width = screenW,
		align = 'center',
	} )
	label_hurray:setFillColor( 0, 0, 0 )
	house_group:insert( label_hurray )

	local house = display.newImageRect( house_group, 'images/house.png', 185, 160)
	house.x = screenW * 0.5
	house.y = screenH * 0.5

	local label_tap = display.newText( {
		text = 'Tap to continue...',
		fontSize = 22,
		font = 'Melapalooza',
		x = screenW * 0.5,
		y = screenH - 30,
		width = screenW,
		align = 'center',
	} )
	label_tap:setFillColor( 0, 0, 0 )
	house_group:insert( label_tap )
end

function showHouse()
	local _max_index = math.ceil( score / 10 )

	utils.debug(_max_index)
	if _max_index > #R.arrays('game_over_text') then
		_max_index = #R.arrays('game_over_text')
	end

	label_hurray.text = R.arrays('game_over_text')[math.random( _max_index or 1 )]
	house_group.alpha = 1
end

function hideHouse()
	house_group.alpha = 0
end

function cat_to_start()
	-- najprv otocime macku do spravnej polohy
	local v, rotation, delta = get_cat_rotation(cat, 100, halfH)

	transition.to( cat, {
		rotation = delta,
		delta = true,
		time = 500,
		onComplete = function ( ... )
			-- teraz ju posunime na spravne miesto
			if not in_house then return end

			transition.to( cat, {
				x = 100,
				y = halfH,
				time = 1200,
				onComplete = function ( ... )
					-- a este ju natocime do zakladnej polohy
					if not in_house then return end

					transition.to( cat, {
						time = 500,
						rotation = 90,
						onComplete = function ( ... )
							if not in_house then return end

							cat:pause( )
						end
					})
				end
			})
		end
	} )
end

function enterHouse()
	-- stop cat movements
	if catMoveTimer ~= nil then
		transition.cancel( cat )
		timer.cancel( catMoveTimer )
		catMoveTimer = nil
	end

	-- touch listener setup
	utils.handlerRemove(background, 'touch')

	in_house = true
	cat_to_start()

	-- hide mouse
	utils.nextFrame(function()
		transition.cancel( mouse )
		mouse.x = -100
		mouse.y = -100
	end)

	-- show house
	showHouse()

	timer.performWithDelay( 1000, function ( ... )
		-- body
		utils.handlerAdd(background, 'touch', function ( self, event )
			if event.phase == 'began' then
				-- it's time to release house!
				releaseHouse()
			end

			return true
		end )
	end)

	timer.performWithDelay( 10, function()
		stop_sound(MOUSE_SOUND_CHANNEL)
		play_sound(sounds.mouse_in_house_sound)
	end )

	-- zobrazovanie pasci
	if trap_timer ~= nil then
		timer.cancel( trap_timer )
		trap_timer = nil
	end

	if trap_object ~= nil then
		-- skovame pascu
		trap_object:removeSelf( )
		trap_object = nil
	end
end

function releaseHouse()
	-- start cat movements
	in_house = false

	-- show mouse
	mouse.x = screenW - 100 
	mouse.y = halfH
	mouse.rotation = -90

	-- hide house
	hideHouse()

	-- posleme macku do utoku
	release_cat_timer = timer.performWithDelay( 1000, releaseCat )

	-- touch listener setup
	utils.handlerRemove(background, 'touch')
	utils.handlerAdd(background, 'touch', backgroundTouchListener)

	-- close door
	closeDoor()

	-- play sound
	play_sound(sounds.door_close_sound)

	-- update food count
	foodCount = 0
	foodCountText.text = 0

	if food ~= nil then
		utils.nextFrame(function ( ... )
			food.isBodyActive = true
		end)
	end

	trap_timer = timer.performWithDelay( 1000, trap_timer_handler, 0)
end

function backgroundTouchListener( self, event )
	--utils.debug('game:backgroundTouchListener', event.phase)

	if event.phase == 'began' then
		local v = math.sqrt( math.abs( mouse.x - event.x ) ^ 2 + math.abs( mouse.y - event.y ) ^ 2 )
		local t = mouseMoveTime * v / mouseMoveDistance
		local rotation = math.deg(math.atan2(event.x - mouse.x, mouse.y - event.y))

		-- fix mouse rotation
		if mouse.rotation > 365 then mouse.rotation = mouse.rotation - 365 end
		if mouse.rotation < 0 then mouse.rotation = mouse.rotation + 365 end

		delta = math.round(rotation - mouse.rotation)
		if delta < -180 then
			delta = delta + 360
		end

		if mouse_transition ~= nil then
			-- utils.debug('game:backgroundTouchListener - cancel transition')
			transition.cancel( mouse_transition )
			mouse_transition = nil
		end

		transition.to( mouse, {
			rotation = delta,
			delta = true,
			time = math.abs( delta ),
		} )

		mouse_transition = transition.to( mouse, {
			x = event.x,
			y = event.y,
			time = t,
			onComplete = function()
				if not mouse_in_move then
					-- mys zastavime iba ked nie je zapnuty flag in move
					-- cize iba ked nemame prst na displeji
					--utils.debug('game:backgroundTouchListener - stop sound')
					mouse:play('stay')
					stop_sound(MOUSE_SOUND_CHANNEL)
					mouse_transition = nil
				end
			end
		} )
	end

	if event.phase == 'began' then
		-- pre zaciatok pohybu spustime animaciu a zvuk
		-- utils.debug('game:backgroundTouchListener - play sound')
		mouse:play('walk')
		play_sound(sounds.mouse_sound, MOUSE_SOUND_CHANNEL, -1)

		-- mys je v pohybe
		mouse_in_move = true
	elseif event.phase == 'moved' then
	elseif event.phase == 'ended' then
		if mouse_transition == nil then
			-- utils.debug('game:backgroundTouchListener - stop sound from in move')
			mouse:play('stay')
			stop_sound(MOUSE_SOUND_CHANNEL)
		end

		mouse_in_move = false
	end

	return true
end

function get_cat_rotation(o, x, y)
	local v = math.sqrt( math.abs( o.x - x ) ^ 2 + math.abs( o.y - y ) ^ 2 )
	local rotation = math.deg(math.atan2(x - o.x, o.y - y))

	if o.rotation > 365 then o.rotation = o.rotation - 365 end
	if o.rotation < 0 then o.rotation = o.rotation + 365 end

	delta = rotation - cat.rotation
	if delta < -180 then
		delta = delta + 360
	end

 return v, rotation, delta
end

function followMouse()
	local mouseX, mouseY = mouse.x, mouse.y

	local v, rotation, delta = get_cat_rotation(cat, mouseX, mouseY)
	
	local n = catMoveTime - score * (65 * math.log(score + 1) / 8)
	if n < 1800 then n = 1800 end
	local t = v * (n / screenW)

	-- pustime mnauknutie
	if math.random(100) > 80 then
		play_sound(sounds.cat_sound, CAT_SOUND_CHANNEL)
	end

	if cat_transition ~= nil then 
		transition.cancel(cat)
		cat_transition = nil
	end

	local n = score
	if n > 100 then n = 100 end

	transition.to( cat, {
		rotation = delta,
		delta = true,
		time = math.abs(delta) * 3 - n,
		onComplete = function ( ... )
		end
	} )

	cat_transition = transition.to( cat, {
		x = mouseX,
		y = mouseY,
		time = t,
		onComplete = followMouse
	})
end

function stopGame()
	utils.handlerRemove(cat, 'collision')
	utils.handlerRemove(background, 'touch')

	if release_cat_timer ~= nil then
		timer.cancel(release_cat_timer)
		release_cat_timer = nil
	end

	if catMoveTimer ~= nil then 
		timer.cancel( catMoveTimer )
		catMoveTimer = nil
	end

	cat:pause()
	mouse:pause()

	transition.cancel( cat )
	transition.cancel( mouse )

	mouse.alpha = 0
	cat.alpha = 0

	-- zastavime zobrazovanie pasci
	if trap_timer ~= nil then 
		timer.cancel( trap_timer )
		trap_timer = nil
	end
end

function mouseCollisionHandler( self, event )
	if event.phase == 'began' then
		stopGame()

		vibrate()

		bang = display.newImageRect( 'images/bang.png', 268, 341 )
		bang.x = mouse.x
		bang.y = mouse.y

		timer.performWithDelay( 1500, function ( ... )
			bang:removeSelf( )
			bang = nil

			composer.gotoScene( 'game-over', {
				params = {
					score = score
				}
			} )
		end )

		-- ulozime score
		if score > settings.best_score then
			settings.best_score = score
			utils.saveSettings(settings)
		end

		send_score(score)

		stop_sound(MOUSE_SOUND_CHANNEL)
		play_sound(sounds.cat_vs_mouse, MOUSE_SOUND_CHANNEL)
	end
end

function send_score(score)
	-- score posleme do gamecenter
	game_network.send_score(R.strings('board_id'), score)
	ga:event("game", "game_network", "score", score)

	-- odomkneme achievements
	local _score_achievements = R.strings('score_achievement_id')[platform_name]

	if _score_achievements ~= nil then
		for i, item in ipairs(_score_achievements) do
			if score >= item.score then
				-- ok, toto mu patri
				game_network.unlock_achievement(item.id)
				ga:event("game", "game_network", "achievement", 'score ' .. item.score)
			end
		end
	end
end

function pointsTransitionFinished( obj )
	obj:removeSelf( )
end

function foodCollisionHandler( self, event )
	if event.phase == 'began' then
		if foodCount >= 9 then
			-- preblikneme pocitadlo jedla
			foodCountText:setFillColor( 1, 0, 0 )
			foodCountText.size = 34

			transition.to( foodCountText, {time = 1200, size = 20, onComplete = function ( ... )
				if foodCountText ~= nil then
					foodCountText:setFillColor( 0, 0, 0 )
				end
			end} )
		end

		if foodCount >= 10 then
			return
		end

		local food_points = self.points

		-- next food
		utils.nextFrame( function()
			if food ~= nil then
				self.isBodyActive = false

				physics.removeBody( food )
				utils.handlerRemove(food, 'collision')
				food:removeSelf( )
				food = nil
			end

			timer.performWithDelay( 500, function ( ... )
				if food ~= nil then
					return
				end

				x = math.random( screenW - 20 ) + 10
				y = math.random( screenH - 20 ) + 10

				if trap_object ~= nil then
					-- ak mame zobrazenu pascu, tak jedlo posunieme tak, aby nekolidovalo s pascou
					if math.abs( trap_object.x - x ) < 20 then
						if trap_object.x > screenW * 0.5 then
							x = x - 100
						else
							x = x + 100
						end
					end

					if math.abs( trap_object.y - y ) < 20 then
						if trap_object.y > screenH * 0.5 then
							y = y - 100
						else
							y = y + 100
						end
					end
				end
				
				-- vytvorime dalsie zradlo
				food = foods.get_next_food(food_group, score, x, y)
				physics.addBody( food, 'static', { bounce = 0, isSensor = true, filter={ categoryBits=4, maskBits=1 } } )

				utils.nextFrame( function ( ... )
					utils.handlerAdd(food, 'collision', foodCollisionHandler)
				end)
			end )
		end )

		-- increment score
		score = score + food_points
		scoreText.text = score

		foodCount = foodCount + 1
		foodCountText.text = foodCount

		if foodCount == 5 then
			-- now you can go to hole
			utils.nextFrame( openDoor )
		end

		-- show points
		local points = display.newText( {
			text = '+' .. food_points,
			fontSize = 18,
			font = 'Melapalooza',
			x = self.x,
			y = self.y
		} )
		points:setFillColor( 0, 0, 0 )

		transition.to( points, {
			alpha = 0,
			y = self.y - 15,
			time = 800,
			onComplete = pointsTransitionFinished
		} )

		play_sound(sounds.eat_sound, EAT_SOUND_CHANNEL)
		vibrate()
	end
end

function releaseCat()
	followMouse()

	cat:play( )

	local t = 2000 - (score * 10)
	if t < 1000 then t = 1000 end

	catMoveTimer = timer.performWithDelay( t, followMouse, 0 )
end

function trap_collision_handler( event )
	-- pasca uz nie je
	if trap_object == nil then return end

	-- zrusime timer skovavania
	if trap_hide_timer ~= nil then
		timer.cancel( trap_hide_timer )
		trap_hide_timer = nil
	end

	-- zastavime pohyb mysky
	if mouse_transition ~= nil then
		transition.cancel( mouse_transition )
		mouse_transition = nil
	end

	-- skovame pascu
	trap_object:removeSelf( )
	trap_object = nil

	utils.handlerRemove(background, 'touch')

	play_sound(sounds.trap_sound)

	timer.performWithDelay( 800, function ( ... )
		utils.handlerAdd(background, 'touch', backgroundTouchListener)
	end )
end

function poison_collision_handler( event )
	-- pasca uz nie je
	if trap_object == nil then return end

	-- zrusime timer skovavania
	if trap_hide_timer ~= nil then
		timer.cancel( trap_hide_timer )
		trap_hide_timer = nil
	end

	-- skovame pascu
	trap_object:removeSelf( )
	trap_object = nil

	mouseMoveDistance = 200
	
	timer.performWithDelay( 1500, function ( ... )
		mouseMoveDistance = 500
	end )
end

function trap_timer_handler(event)
	-- este mame pascu zobrazenu
	if trap_object ~= nil then return end

	local created = false

	if score > 30 then
		if math.random(100) > 90 then
			-- zobrazime jed
			trap_object = trap.get_poison(trap_group, poison_collision_handler)
			created = true
		end
	end

	if trap_object == nil and score > 60 then
		if math.random(100) > 90 then
			-- zobrazime pascu
			trap_object = trap.get_trap(trap_group, trap_collision_handler)
			created = true
		end
	end

	if created then
		-- pascu skovame po urcitom case
		local t = 3000 + math.random(2000)
		trap_hide_timer = timer.performWithDelay( t, function ( ... )
			-- pasca uz nie je
			if trap_object == nil then return end

			-- skovame pascu
			trap_object:removeSelf( )
			trap_object = nil
		end )
	end
end

function startGame()
	mouse.x = screenW - 100 
	mouse.y = halfH
	mouse.rotation = -90

	cat.x = 100
	cat.y = halfH
	cat.rotation = 90

	score = 0
	scoreText.text = 0

	foodCount = 0
	foodCountText.text = 0

	food = foods.get_next_food(food_group, score, halfW, halfH)
	physics.addBody( food, 'static', { bounce = 0, isSensor = true, filter={ categoryBits=4, maskBits=1 } } )
	utils.handlerAdd(food, 'collision', foodCollisionHandler)

	utils.handlerAdd(cat, 'collision', mouseCollisionHandler)
	utils.handlerAdd(background, 'touch', backgroundTouchListener)

	release_cat_timer = timer.performWithDelay( 1000, releaseCat )

	closeDoor()

	-- loadnem reklamu, aby sa potom rychlejsie zobrazila
	ads.hide()
	ads.load( "interstitial", { testMode=false } )

	-- zobrazovanie pasci
	trap_timer = timer.performWithDelay( 1000, trap_timer_handler, 0)
end

function scene:create( event )
	local sceneGroup = self.view

	background = display.newImageRect( 'images/background.jpg', screenW, screenH )
	background.anchorX = 0
	background.anchorY = 0
	background.x = 0
	background.y = 0

	-- mouse animation
	local sheetWidth = 580
	local sheetHeight = 485
	local imageSheetOptions = {
		width = sheetWidth / 10,
		height = sheetHeight / 4,
		numFrames = 39,
		sheetContentWidth = sheetWidth,
		sheetContentHeight = sheetHeight
	}

	local imageSheet = graphics.newImageSheet( 'images/mouse.png', imageSheetOptions )

	mouse = display.newSprite( imageSheet, {
		{name = 'stay', start = 1, count = 20},
		{name = 'walk', start = 21, count = 19},
	} )
	mouse.anchorY = 0.3

	-- cat animation
	local sheetWidth = 1160
	local sheetHeight = 485
	local imageSheetOptions = {
		width = sheetWidth / 10,
		height = sheetHeight / 2,
		numFrames = 19,
		sheetContentWidth = sheetWidth,
		sheetContentHeight = sheetHeight
	}

	local imageSheet = graphics.newImageSheet( 'images/cat.png', imageSheetOptions )

	cat = display.newSprite( imageSheet, {
		{name = 'walk', start = 1, count = 19},
	} )
	cat.anchorY = 0.5

	doorOpen = display.newImageRect( 'images/hole.png', 40, 25)
	doorOpen.x, doorOpen.y = -100, -100

	doorClosed = display.newImageRect( 'images/hole-closed.png', 40, 25)
	doorClosed.x, doorClosed.y = 380, 60

	-- score
	local score_label = display.newText( {
		text = 'SCORE',
		x = screenW - 192,
		y = 20,
		fontSize = 20,
		font = 'Melapalooza'
	} )
	score_label:setFillColor( 0, 0, 0 )

	local score_background = display.newImageRect( 'images/score-bg.png', 57, 43 )
	score_background.x = screenW - 140
	score_background.y = 20

	scoreText = display.newText( {
		text = '0',
		x = screenW - 140,
		y = 20,
		fontSize = 20,
		font = 'Melapalooza'
	} )
	scoreText:setFillColor( 0, 0, 0 )

	-- food
	local food_count_label = display.newText( {
		text = 'FOOD',
		x = screenW - 78,
		y = 20,
		fontSize = 20,
		font = 'Melapalooza'
	} )
	food_count_label:setFillColor( 0, 0, 0 )

	local food_count_background = display.newImageRect( 'images/food-bg.png', 57, 43 )
	food_count_background.x = screenW - 30
	food_count_background.y = 20

	foodCountText = display.newText( {
		text = '0',
		x = screenW - 30,
		y = 20,
		fontSize = 20,
		font = 'Melapalooza'
	} )
	foodCountText:setFillColor( 0, 0, 0 )

	-- add all items in to group
	sceneGroup:insert( background )

	sceneGroup:insert(doorClosed)
	sceneGroup:insert(doorOpen)

	sceneGroup:insert( score_background )
	sceneGroup:insert( score_label )
	sceneGroup:insert( scoreText )

	sceneGroup:insert( food_count_background )
	sceneGroup:insert( food_count_label )
	sceneGroup:insert( foodCountText )

	food_group = display.newGroup( )
	trap_group = display.newGroup( )

	sceneGroup:insert( food_group )
	sceneGroup:insert( trap_group )

	sceneGroup:insert( mouse )
	sceneGroup:insert( cat )

	-- zobrazujeme noru, ked tam myska vbehne
	house_group = display.newGroup( )
	sceneGroup:insert( house_group )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		inCollision = false

		physics.start()
		physics.setGravity( 0, 0 )
		--physics.setDrawMode( 'hybrid' )

		mouse.alpha = 1
		cat.alpha = 1

		physics.addBody( mouse, 'dynamic',
			{
				bounce = 0, isSensor = true, filter={ categoryBits=1, maskBits=62 },
				shape = {   13.5, -31  ,  15.5, -18  ,  15.5, 1  ,  9.5, 13  ,  -6.5, 2  ,  -9.5, -17  ,  -5.5, -33  ,  3.5, -45  }
			},
			{
				bounce = 0, isSensor = true, filter={ categoryBits=1, maskBits=62 },
				shape = {   -6.5, 2  ,  9.5, 13  ,  0.5, 13  }
			}
		)

		physics.addBody( cat, 'dynamic', 
			{
				bounce = 0, isSensor = true, radius=50, filter={ categoryBits=2, maskBits=1 },
				shape = {   -27.5, 35  ,  -39.5, -4  ,  -24.5, -63  ,  -8.5, -79  ,  17.5, -79  ,  38.5, 15  ,  26.5, 35  ,  -0.5, 48  }
			},
			{
				bounce = 0, isSensor = true, radius=50, filter={ categoryBits=2, maskBits=1 },
				shape = {   38.5, 15  ,  17.5, -79  ,  39.5, -63  }
			} 
		)
		physics.addBody( doorOpen, 'static', { bounce = 0, isSensor = true, filter={ categoryBits=8, maskBits=1 } } )

		prepare_house()
	elseif phase == "did" then
		startGame()

		if settings.music then
			audio.play( sounds.music_sound, {channel = MUSIC_CHANNEL, loops = -1} )
			audio.setVolume( 0.75, {channel = MUSIC_CHANNEL} )
		end

		ga:view('game')
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		physics.stop( )

		if catTimer ~= nil then
			timer.cancel( catTimer )
			catTimer = nil
		end

		stopGame()
	elseif phase == "did" then
		-- Called when the scene is now off screen
		if food ~= nil then 
			food:removeSelf( )
			food = nil
		end

		if bang ~= nil then 
			bang:removeSelf( )
			bang = nil
		end

		-- ak je zobrazena pasca, tak ju odstranime
		if trap_object ~= nil then
			trap_object:removeSelf( )
			trap_object = nil
		end

		stop_sound(MOUSE_SOUND_CHANNEL)

		if settings.music then
			audio.fadeOut( MUSIC_CHANNEL, 1500 )
		end
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene